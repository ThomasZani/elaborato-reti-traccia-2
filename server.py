import socketserver
import sys
import http.server
import signal

def kill_handler(signal, frame):
    try:
      if( server ):
        server.server_close()
    finally:
      sys.exit(0)

signal.signal(signal.SIGINT, kill_handler)

if sys.argv[1:]:
  port = int(sys.argv[1])
else:
  port = 8080

server = socketserver.ThreadingTCPServer(('',port), http.server.SimpleHTTPRequestHandler )
server.daemon_threads = True 
server.allow_reuse_address = True  

try:
  while True:
    print("Server online, press control+C to close it")
    sys.stdout.flush()
    server.serve_forever()
except KeyboardInterrupt:
  pass

server.server_close()